<?php

define('APP_ENV', getenv('APP_ENV') ?: 'development'); // development || staging || production

if (APP_ENV === 'production' || APP_ENV === 'staging') {
    define('APP_DB_USER', getenv('TOOL_TOOLSDB_USER'));
    define('APP_DB_PASSWORD', getenv('TOOL_TOOLSDB_PASSWORD'));
    define('APP_DB_HOST', 'tools.db.svc.wikimedia.cloud');
    define('APP_DB_NAME', getenv('TOOL_TOOLSDB_USER') . '__cartografiaschaco');
} else {
    define('APP_DB_USER', 'root');
    define('APP_DB_PASSWORD', '');
    define('APP_DB_HOST', 'localhost');
    define('APP_DB_NAME', 'cartografiaschaco.com');
}

define('APP_HOME_URL', getenv('APP_HOME_URL') ?: 'https://localhost/cartografiaschaco/');

define('APP_CONTACT_MAIL', getenv('APP_CONTACT_MAIL'));

define('APP_DEBUG_MODE', true);

// define('WIKI_USER', '');
// define('WIKI_PASSWORD', '');
