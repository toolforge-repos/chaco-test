This tool is an example php tool for the Toolforge environment using the build
service.

See more details here:
https://wikitech.wikimedia.org/wiki/Help:Toolforge/My_first_Buildpack_PHP_tool
